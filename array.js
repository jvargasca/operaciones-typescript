"use strict";
exports.__esModule = true;
exports.elementoEliminado = exports.nuevaLongitud = exports.ultimo = exports.Dispositivos = void 0;
//1
var Dispositivos;
exports.Dispositivos = Dispositivos;
exports.Dispositivos = Dispositivos = ["Iphone", "Laptop", "Tablet", "Smartwatch"];
//2
Dispositivos.forEach(function (elemento, indice, array) {
    console.log(elemento, indice);
});
console.log("\n");
//3
var ultimo = Dispositivos.pop();
exports.ultimo = ultimo;
//4
var nuevaLongitud = Dispositivos.unshift('Alexa');
exports.nuevaLongitud = nuevaLongitud;
//5
var elementoEliminado = Dispositivos.splice(4);
exports.elementoEliminado = elementoEliminado;
