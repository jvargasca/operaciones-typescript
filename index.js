"use strict";
exports.__esModule = true;
var boolean_1 = require("./boolean");
var number_1 = require("./number");
var type_1 = require("./type");
var array_1 = require("./array");
/*array*/
console.log("array", "\n");
console.log("crear array: ", array_1.Dispositivos.length, "\n");
console.log("Eliminar ultimo elemento", array_1.Dispositivos, "\n");
console.log("Añadir Elemento al inicio", array_1.Dispositivos, "\n");
console.log("Eliminar elemento por posicion", array_1.Dispositivos);
console.log("\n");
/*string*/
console.log("string", "\n");
console.log(type_1.nombre);
console.log(type_1.apellidos);
console.log(type_1.cedula);
console.log(type_1.celular);
console.log(type_1.direccion);
console.log(type_1.concatenar);
console.log(type_1.variable);
console.log(type_1.mayuscula);
console.log(type_1.letra);
console.log(type_1.porcion);
console.log("\n");
/*number*/
console.log("number", "\n");
console.log("La suma es:", number_1.resultadosuma);
console.log("La resta es:", number_1.resultadoresta);
console.log("La multiplicaci\u00F3n es:", number_1.resultadomulti);
console.log("La divisi\u00F3n es:", number_1.resultadodivi);
console.log("La potencia es:", number_1.resultadopote);
console.log("\n");
/*booleans*/
console.log("booleans", "\n");
console.log(boolean_1.booleans);
console.log(boolean_1.condicional);
console.log(boolean_1.trueNum); // 1
console.log(boolean_1.falseNum); // 0
console.log(boolean_1.trueStr); //true
console.log(boolean_1.falseStr); //false
console.log(boolean_1.var1);
console.log(boolean_1.var2);
console.log(boolean_1.var3);
