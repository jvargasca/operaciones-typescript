export {nombre,apellidos,cedula,celular,direccion,concatenar,variable,mayuscula,letra,porcion}
let nombre: string ="monik";
let apellidos: string = "Alvear";
let cedula: string = "34561728";
let celular: string = "311728390";
let direccion: string = "carrera 73b# 43a 21";

/*concatenar*/
let concatenar = nombre.concat(apellidos.toString());
let variable = cedula.concat(', ', celular);
let mayuscula = nombre.toUpperCase();
let letra = apellidos.charAt(2);
let porcion =direccion.substring(3);
