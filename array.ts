export{Dispositivos,ultimo,nuevaLongitud,elementoEliminado}
//1
let Dispositivos: string[];
Dispositivos = ["Iphone", "Laptop", "Tablet", "Smartwatch"]

//2
Dispositivos.forEach(function(elemento, indice, array) {
    console.log(elemento,indice);
})
console.log("\n");
//3
let ultimo = Dispositivos.pop()

//4

let nuevaLongitud = Dispositivos.unshift('Alexa') 

//5

let elementoEliminado = Dispositivos.splice(4)

