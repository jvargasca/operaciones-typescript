import { booleans,condicional,trueNum,falseNum,trueStr,falseStr, var1,var2,var3} from "./boolean";
import {resultadosuma,resultadoresta,resultadomulti,resultadodivi,resultadopote} from "./number";
import{nombre,apellidos,cedula,celular,direccion,concatenar, variable, mayuscula, letra, porcion} from "./type";
import{Dispositivos,ultimo,nuevaLongitud,elementoEliminado} from "./array";

/*array*/
console.log("array","\n");
console.log("crear array: ",Dispositivos.length, "\n");
console.log("Eliminar ultimo elemento",Dispositivos,"\n");
console.log("Añadir Elemento al inicio", Dispositivos,"\n")
console.log("Eliminar elemento por posicion",Dispositivos);
console.log("\n");

/*string*/
console.log("string","\n");
console.log(nombre);
console.log(apellidos);
console.log(cedula);
console.log(celular);
console.log(direccion);
console.log(concatenar)
console.log(variable)
console.log(mayuscula)
console.log(letra)
console.log(porcion)
console.log("\n");
/*number*/
console.log("number","\n");
console.log(`La suma es:`,resultadosuma);
console.log(`La resta es:`,resultadoresta);
console.log(`La multiplicación es:`,resultadomulti);
console.log(`La división es:`,resultadodivi);
console.log(`La potencia es:`,resultadopote);
console.log("\n");
/*booleans*/
console.log("booleans","\n");
console.log(booleans);
console.log(condicional);
console.log(trueNum);  // 1
console.log(falseNum); // 0
console.log(trueStr)      //true
console.log(falseStr)     //false
console.log(var1);
console.log(var2);
console.log(var3);
